package com.blackhawks.dijkstraimpl.main;

import com.blackhawks.dijkstraimpl.test.TestDijkstra;

public class Main {

	public static void main(String args[]){
		TestDijkstra testDijkstra = new TestDijkstra();
		testDijkstra.testExec();
		
	}
}
