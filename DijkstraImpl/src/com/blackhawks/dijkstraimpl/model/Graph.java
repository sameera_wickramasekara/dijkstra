package com.blackhawks.dijkstraimpl.model;

import java.util.ArrayList;

public class Graph {

	private ArrayList<Vertex> vertxList;
	private ArrayList<Edge> edgeList;

	public ArrayList<Vertex> getVertxList() {
		return vertxList;
	}

	public void setVertxList(ArrayList<Vertex> vertxList) {
		this.vertxList = vertxList;
	}

	public ArrayList<Edge> getEdgeList() {
		return edgeList;
	}

	public void setEdgeList(ArrayList<Edge> edgeList) {
		this.edgeList = edgeList;
	}

}
