package com.blackhawks.dijkstraimpl.model;

public class Vertex implements Comparable<Vertex>{
	private String id;
	private String name;
	private int distance = Integer.MAX_VALUE;// at first all vertexes have infinite distance value

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Vertex(String id, String name) {
		this.name = name;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		//System.out.println("Node" + name+" distance :"+getDistance());
		return "Node" + name ;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Vertex)) {
			return false;
		} else if (obj == this) {
			return true;
		} else {
			Vertex v = (Vertex) obj;
			return v.getId().equals(this.getId()); 
		}

	}

	public int compareTo(Vertex arg0) {
		if(arg0.getDistance()>this.distance){
			return -1;
		}else if(arg0.getDistance()<this.distance){
			return 1;
		}else{
			return 0;
		}
	}

}
