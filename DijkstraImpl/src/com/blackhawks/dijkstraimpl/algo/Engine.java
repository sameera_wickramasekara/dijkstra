package com.blackhawks.dijkstraimpl.algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.blackhawks.dijkstraimpl.model.Edge;
import com.blackhawks.dijkstraimpl.model.Graph;
import com.blackhawks.dijkstraimpl.model.Vertex;

public class Engine {
	/*
	 * declaring the variables
	 */
	ArrayList<Vertex> vertexList;
	ArrayList<Edge> edgeList;
	Graph graph;
	Vertex source;
	Map<Vertex, Vertex> path;
	Set<Vertex> solution;

	/*
	 * initialize from constructor
	 */
	public Engine(Graph graph) {
		this.graph = graph;
		edgeList = graph.getEdgeList();// all the edges in the graph
		vertexList = graph.getVertxList();// all the vertices of the graph
		solution = new HashSet<Vertex>();// store the lowest distances from the
											// source
		path = new HashMap<Vertex, Vertex>();// store the path
	}

	/*
	 * set the source node of journey
	 */
	public void setSource(Vertex source) {

		this.source = source;
		source.setDistance(0);
	}

	/*
	 * main calculations are done in this method
	 */
	public void execute() {

		// consider all the vertices
		while (!(vertexList.isEmpty())) {

			// get the immidiate neighbours from the source
			ArrayList<Vertex> neighbours = getNeighbours(source);
			// remove the considerd node from the vertex list
			vertexList.remove(source);

			if (!(neighbours.isEmpty())) {
				// find the vertex with the lowest distance from source
				Vertex lowestDistaneNode = findLowestDistaneNode(vertexList);
				// set lowest distance node as the source
				source = lowestDistaneNode;

				// in case there is only one vertex left in the list and it has
				// no unprocessd neighbours
				// that vertex is added to the solutions list
			} else {
				if (!(vertexList.isEmpty())) {
					for (int i = 0; i < vertexList.size(); i++) {
						solution.add(vertexList.get(i));
					}

					vertexList.clear();
				}
			}
			// solution containing the distances
			solution.add(source);

		}

	}

	private Vertex findLowestDistaneNode(ArrayList<Vertex> neighbours) {

		Collections.sort(neighbours);
		return neighbours.get(0);

	}

	/*
	 * get the immediate nodes from parameter node
	 */
	private ArrayList<Vertex> getNeighbours(Vertex node) {
		ArrayList<Vertex> neighbours = new ArrayList<Vertex>();

		for (Edge edge : edgeList) {

			if (edge.getSource().equals(node)) {

				Vertex neighberVertex = edge.getDestination();

				if (neighberVertex.getDistance() == Integer.MAX_VALUE) {

					neighberVertex.setDistance(node.getDistance()
							+ edge.getWeight());

					// store the lowest distance vertex for determining the path
					if (vertexList.contains(neighberVertex)) {
						path.put(neighberVertex, node);
					}

				} else if (neighberVertex.getDistance() > node.getDistance()
						+ edge.getWeight()) {
					neighberVertex.setDistance(node.getDistance()
							+ edge.getWeight());
					if (vertexList.contains(neighberVertex)) {
						if (path.containsKey(neighberVertex)) {
							path.replace(neighberVertex, node);
						}
					}

				}
				if (vertexList.contains(neighberVertex)) {
					neighbours.add(neighberVertex);
				}
			} else if (edge.getDestination().equals(node)) {

				Vertex neighberVertex = edge.getSource();

				if (neighberVertex.getDistance() == Integer.MAX_VALUE) {

					neighberVertex.setDistance(node.getDistance()
							+ edge.getWeight());
					if (vertexList.contains(neighberVertex)) {
						path.put(neighberVertex, node);
					}

				} else if (neighberVertex.getDistance() > node.getDistance()
						+ edge.getWeight()) {

					neighberVertex.setDistance(node.getDistance()
							+ edge.getWeight());
					if (vertexList.contains(neighberVertex)) {
						if (path.containsKey(neighberVertex)) {
							path.replace(neighberVertex, node);
						}
					}
				}
				if (vertexList.contains(neighberVertex)) {
					neighbours.add(neighberVertex);
				}
			}
		}

		return neighbours;

	}

	// show path from vertex to vertex
	public void showPath(Vertex destination) {
		Vertex temp = destination;
		ArrayList<Vertex> shortestPath = new ArrayList<Vertex>();
		while (path.containsKey(temp)) {
			shortestPath.add(path.get(temp));

			temp = path.get(temp);

		}
		Collections.reverse(shortestPath);
		for (Vertex p : shortestPath) {
			System.out.print(p.toString() + "-->");
		}
		System.out.print(destination.toString());

	}

	// show shortest distances
	public void showShortestDistances() {
		for (Vertex p : solution) {
			System.out.println(p.getName() + " distance:" + p.getDistance());
		}
	}

}
