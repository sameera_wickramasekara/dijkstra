package com.blackhawks.dijkstraimpl.test;

import java.util.ArrayList;

import com.blackhawks.dijkstraimpl.algo.Engine;
import com.blackhawks.dijkstraimpl.model.Edge;
import com.blackhawks.dijkstraimpl.model.Graph;
import com.blackhawks.dijkstraimpl.model.Vertex;

public class TestDijkstra {

	ArrayList<Vertex> vertexList;
	ArrayList<Edge> edgeList;
	Graph graph;

	/*
	 * Test method
	 */
	public void testExec() {

		// initialize graph
		init();
		Engine engine = new Engine(graph);// set graph to engine
		Vertex a = new Vertex("1", "A");// define source node
		Vertex f = new Vertex("6", "F");// define destination node

		engine.setSource(a);// set source to engine
		engine.execute();// calculate
		engine.showShortestDistances();
		engine.showPath(f);

	}

	/*
	 * initialize the test Values
	 */
	private void init() {
		// declare the vertexes
		Vertex a = new Vertex("1", "A");
		Vertex b = new Vertex("2", "B");
		Vertex c = new Vertex("3", "C");
		Vertex d = new Vertex("4", "D");
		Vertex e = new Vertex("5", "E");
		Vertex f = new Vertex("6", "F");

		// add vertexes to the arrayList
		vertexList = new ArrayList<Vertex>();

		vertexList.add(a);
		vertexList.add(b);
		vertexList.add(c);
		vertexList.add(d);
		vertexList.add(e);
		vertexList.add(f);

		// declare edges and connect vetexes
		Edge e1 = new Edge("1", a, b, 2);
		Edge e2 = new Edge("2", a, c, 3);
		Edge e3 = new Edge("3", b, c, 2);
		Edge e4 = new Edge("4", b, d, 1);
		Edge e5 = new Edge("5", e, c, 1);
		Edge e6 = new Edge("6", b, e, 3);
		Edge e7 = new Edge("7", d, e, 2);
		Edge e8 = new Edge("8", b, f, 3);
		Edge e9 = new Edge("9", d, f, 1);

		Edge e10 = new Edge("10", e, f, 2);

		// add edges to edgeList
		edgeList = new ArrayList<Edge>();
		edgeList.add(e1);
		edgeList.add(e2);
		edgeList.add(e3);
		edgeList.add(e4);
		edgeList.add(e5);
		edgeList.add(e6);
		edgeList.add(e7);
		edgeList.add(e8);
		edgeList.add(e9);
		edgeList.add(e10);

		// prepare graph
		graph = new Graph();
		graph.setEdgeList(edgeList);
		graph.setVertxList(vertexList);

	}

}
